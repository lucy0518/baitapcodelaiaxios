const DSSV_LOCAlSTORAGE = "DSSV_LOCAlSTORAGE";

var dssv = [];

var dssvJson = localStorage.getItem("DSSV");

if (dssvJson != null) {
  console.log("yes");
  dssv = JSON.parse(dssvJson);
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    dssv[index] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.matkhau,
      sv.email,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }
  renderDSSV(dssv);
}

// function themSV() {
//   var newSV = layThongTinTuForm();

// var isValid =
//   validator.kiemTraRong(
//     newSV.ma,
//     "spanMaSV",
//     "Mã sinh viên không được để rỗng"
//   ) &&
//   validator.kiemTraRong(
//     newSV.ten,
//     "spanTenSV",
//     " Tên sinh viên không được để rỗng "
//   ) &
//     validator.kiemTraDoDai(
//       newSV.ma,
//       "spanMaSV",
//       "Mã sinh viên phải bao gồm 4 kí tự",
//       4,
//       4
//     );

// isValid =
//   isValid &
//   validator.kiemTraRong(
//     newSV.ten,
//     "spanTenSV",
//     "Tên sinh viên không được để rỗng"
//   );

// isValid =
//   isValid &
//   validator.kiemTraRong(
//     newSV.email,
//     "spanEmailSV",
//     "Email không được để rỗng"
//   );

// if (isValid) {
//   dssv.push(newSV);

//   var dssvJson = JSON.stringify(dssv);

//   localStorage.setItem("DSSV :", dssvJson);

//   renderDSSV(dssv);

//   console.log("dssv: ", dssv);
// }
// }

function xoaSinhVien(id) {
  console.log(id);

  var index = timKiemViTri(id, dssv);
  if (index != -1) {
    console.log(index);
    dssv.splice(index, 1);

    renderDSSV(dssv);
  }
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
}
